@extends('layout.master')
@section('judul')
    Halaman Edit Cast
@endsection

    @section('content')
    <form action="/cast/{{$cast->id}}" method="post">
        @csrf
        @method('PUT')
        <div class="mb-3">
          <label for="exampleInputEmail1" class="form-label">Nama Cast</label>
          <input type="text" name="nama" value="{{$cast->nama}}" class="form-control">
        </div>
        @error('nama')
            <div class="alert alert-danger">{{ $message }}</div>
        @enderror
        <div class="mb-3">
            <label for="exampleInputEmail1" class="form-label">Umur</label>
            <input type="number" name="umur" value="{{$cast->umur}}" class="form-control">
          </div>
          @error('umur')
              <div class="alert alert-danger">{{ $message }}</div>
          @enderror
        <div class="mb-3">
          <label for="exampleInputPassword1" class="form-label">Biodata</label>
          <textarea name="bio" class="form-control" cols="30" rows="10">{{$cast->bio}}</textarea>
        </div>
        @error('bio')
        <div class="alert alert-danger">{{ $message }}</div>
        @enderror
        <button type="submit" class="btn btn-primary">Submit</button>
      </form>
    @endsection
