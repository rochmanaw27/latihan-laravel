@extends('layout.master')
@section('judul')
    Halaman Tambah Cast
@endsection

    @section('content')
    <form action="/cast" method="post">
        @csrf
        <div class="mb-3">
          <label for="exampleInputEmail1" class="form-label">Nama Cast</label>
          <input type="text" name="nama" class="form-control">
        </div>
        @error('nama')
            <div class="alert alert-danger">{{ $message }}</div>
        @enderror
        <div class="mb-3">
            <label for="exampleInputEmail1" class="form-label">Umur</label>
            <input type="number" name="umur" class="form-control">
          </div>
          @error('umur')
              <div class="alert alert-danger">{{ $message }}</div>
          @enderror
        <div class="mb-3">
          <label for="exampleInputPassword1" class="form-label">Biodata</label>
          <textarea name="bio" class="form-control" cols="30" rows="10"></textarea>
        </div>
        @error('bio')
        <div class="alert alert-danger">{{ $message }}</div>
        @enderror
        <button type="submit" class="btn btn-primary">Submit</button>
      </form>
    @endsection
