@extends('layout.master')
@section('judul')
    Halaman List Cast
@endsection

    @section('content')
    <a class="btn btn-success btn-sm mb-3" href="/cast/create">Tambah Data</a>
    <table class="table">
    <thead class="table-dark">
      <tr>
        <th scope="col">#</th>
        <th scope="col">Nama</th>
        <th scope="col">Umur</th>
        <th scope="col">Biodata</th>
        <th scope="col">Action</th>
      </tr>
    </thead>
    <tbody>
      @forelse($cast as $key =>$item)
        <tr>
            <td>{{$key+1}}</td>
            <td>{{$item->nama}}</td>
            <td>{{$item->umur}}</td>
            <td>{{$item->bio}}</td>
            <td>
                <form action="/cast/{{$item->id}}" method="post">
                    <a class="btn btn-info btn-sm" href="/cast/{{$item->id}}">Detail</a>
                    <a class="btn btn-warning btn-sm" href="/cast/{{$item->id}}/edit">Edit</a>
                    @csrf
                    @method('delete')
                    <input type="submit" class="btn btn-danger btn-sm" value="Delete">
                </form>
            </td>
        </tr>
      @empty
        <tr>
            <td>Data Masih Kosong</td>
        </tr>
      @endforelse
    </tbody>
  </table>
  @endsection
