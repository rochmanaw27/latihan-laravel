@extends('layout.master')
@section('judul')
    Halaman Form
@endsection

    @section('content')
        <h1>Halaman Form</h1>
        <form action="/welcome" method="post">
            @csrf
            <label for="fname">First name:</label><br>
            <input type="text" id="fname" name="fname"><br><br>

            <label for="lname">Last name:</label><br>
            <input type="text" id="lname" name="lname"><br><br>

            <label for="fname">Gender:</label><br>
            <input type="radio" id="male" name="gender" value="Male">
            <label for="male">Male</label><br>
            <input type="radio" id="female" name="gender" value="Female">
            <label for="female">Female</label><br>
            <input type="radio" id="other" name="gender" value="Other">
            <label for="other">Other</label><br><br>

            <label for="nationality">Nationality:</label><br>
            <select id="nationality" name="nationality">
              <option value="Indonesia">Indonesia</option>
              <option value="Amerika">Amerika</option>
              <option value="Inggris">Inggris</option>
            </select><br><br>

            <label for="lang">Language Spoke:</label><br>
            <input type="checkbox" id="indonesia" name="indonesia" value="Bahasa Indonesia">
            <label for="indonesia"> Bahasa Indonesia</label><br>
            <input type="checkbox" id="english" name="english" value="English">
            <label for="english"> English</label><br>
            <input type="checkbox" id="other" name="other" value="Other">
            <label for="other"> Other</label><br><br>

            <label for="bio">Bio:</label><br>
            <textarea name="bio" rows="10" cols="30"></textarea><br><br>

            <input type="submit" value="Sign Up">
        </form>
        @endsection
