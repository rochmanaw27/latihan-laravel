@extends('layout.master')
@section('judul')
    Halaman Index
@endsection

@section('content')
    <h1><b>Media Online</b></h1>
        <p><b>Social Media Developer</b></p>
        <p>Belajar dan berbagi agar hidup menjadi lebih baik</p>
        <p><b>Benefit Join di Media Online</b></p>
        <ul>
            <li>Mendapatkan motivasi dari sesama para Developer</li>
            <li>Sharing knowledge</li>
            <li>Dibuat oleh calon web developer terbaik</li>
        </ul>
        <p><b>Cara Bergabung ke Media Online</b></p>
        <ol>
            <li>Mengunjungi Website ini</li>
            <li>Mendaftarkan di <a href ="/register">Form Sign Up</a></li>
            <li>Selesai</li>
        </ol>

@endsection

