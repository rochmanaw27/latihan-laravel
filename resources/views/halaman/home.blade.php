<!DOCTYPE html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
    <head>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1">

        <title>Laravel</title>

        <!-- Fonts -->
        <link href="https://fonts.googleapis.com/css2?family=Nunito:wght@200;600&display=swap" rel="stylesheet">
    </head>
    <body>
        <h1>SELAMAT DATANG {{$fname}} {{$lname}}</h1>
        <p><b>Terima Kasih telah bergabung di website Kami, Media belajar kita bersama!</b></p>

    </body>
</html>
