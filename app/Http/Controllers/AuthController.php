<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class AuthController extends Controller
{
    public function form(){
        return view('halaman.form');
    }

    public function kirim(Request $request){
        $fname = $request["fname"];
        $lname = $request["lname"];
        $nationality = $request["nationality"];
        $gender = $request["gender"];
        $indonesia = $request["indonesia"];
        $english = $request["english"];
        $other = $request["other"];
        $bio = $request["bio"];

        return view('halaman.home', compact('fname','lname','nationality','gender','indonesia','english','other','bio'));
    }
}
